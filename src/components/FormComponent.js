import React from "react";

import { TextField } from "@material-ui/core";

export const TextInput = ({
  input,
  label,
  id,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <TextField
    label={label}
    error={touched && invalid}
    helperText={touched && error}
    {...input}
    {...custom}
  />
);
