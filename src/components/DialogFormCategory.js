import React from "react";
import { reduxForm, Field } from "redux-form";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  MenuItem
} from "@material-ui/core";

import { TextInput } from "./FormComponent";

// validation
import { createValidator, required } from "./../constants/validation";

const FormDialogCategory = props => {
  React.useEffect(() => { }, []);
  const { handleCloseDialog, handleOpenDialog, handleSubmit } = props;

  return (
    <div>
      <Dialog
        open={handleOpenDialog}
        maxWidth="sm"
        fullWidth={true}
        onClose={e => handleCloseDialog(e)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">{document.title}</DialogTitle>

        <form onSubmit={handleSubmit} autoComplete="off">
          <DialogContent>

            <Field
              name="type_name"
              component={TextInput}
              label="ชื่อหมวดหมู่"
              margin="normal"
              variant="outlined"
              type="text"
              fullWidth
            />

            <Field
              name="active_flag"
              select
              label="สถานะ"
              component={TextInput}
              margin="normal"
              variant="outlined"
              fullWidth
            >
              <MenuItem value="">None</MenuItem>
              <MenuItem value={true}>แสดงผล</MenuItem>
              <MenuItem value={false}>ไม่แสดงผล</MenuItem>
            </Field>
          </DialogContent>
          <DialogActions>
            <Button type="submit" color="primary">
              {`Submit`}
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};
const validate = values => {
  return createValidator({
    title: required,
    content: required
  })(values);
};
export default reduxForm({
  form: "category-form",
  validate,
  enableReinitialize: true,
  destroyOnUnmount: false
})(FormDialogCategory);
