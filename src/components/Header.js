/**
 * Description: Function Component สำหรับแสดงด้านบนของหน้าจอหลัก
 */
import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import { Menu, MenuItem } from "@material-ui/core/";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
//เรียกใช้งาน icon
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  }
}));

const Header = props => {
  const { styles, onSidebarToggle } = props;
  const classes = useStyles();
  //
  let [anchorEl, setAnchorEl] = React.useState(null); //ใช้ React Hook ในการจัดการ state ของ
  const style = {
    appBar: {
      position: "fixed",
      top: 0,
      overflow: "hidden",
      maxHeight: 57
    },
    menuButton: { marginLeft: 10 },
    iconRightContainer: { marginLeft: 20 }
  };
  /**
   *
   */

  return (
    <React.Fragment>
      <AppBar style={{ ...styles, ...style.appBar }}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            aria-label="Menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            ร้านข้าวต้มอนันต์
          </Typography>
          <div>
            <IconButton
              aria-owns={Boolean(anchorEl) ? "menu-user" : undefined}
              aria-haspopup="true"
              onClick={event => setAnchorEl(event.currentTarget)}
            >
              <AccountCircleIcon />
            </IconButton>
            <Menu
              id="menu-user"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right"
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right"
              }}
              open={Boolean(anchorEl)}
              onClose={() => setAnchorEl(null)}
            >
              <MenuItem onClick={() => setAnchorEl(null)}>
                ข้อมูลผู้ใช้งาน
              </MenuItem>
              <MenuItem onClick={() => setAnchorEl(null)}>ออกจากระบบ</MenuItem>
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
};
Header.propTypes = {
  styles: PropTypes.object, //style ของ Header
  handleSidebarToggle: PropTypes.func //event handler ในการจัดการเมนูด้านข้าง
};

export default Header;
