import React from "react";
import PropTypes from "prop-types";

import IconButton from "@material-ui/core/IconButton";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

//icons
import FullScreenIcon from "@material-ui/icons/Fullscreen";
import FullScreenExitIcon from "@material-ui/icons/FullscreenExit";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  appBar: {
    position: "relative"
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  }
}));

const LaneHeader = props => {
  let classes = useStyles();
  const [open, setOpen] = React.useState(false); //กำหนด Hook ในการจัดการเปิดปิด dialog
  return (
    <div>
      <header
        style={{
          borderBottom: "2px solid #c5c5c5",
          paddingBottom: 6,
          marginBottom: 10,
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between"
        }}
      >
        <div style={{ fontSize: 14, fontWeight: "bold" }}>{props.title}</div>
        {props.label && (
          <div style={{ width: "30%", textAlign: "right", fontSize: 13 }}>
            <IconButton
              onClick={() => {
                setOpen(true);
              }}
            >
              <FullScreenIcon />
            </IconButton>
            <Dialog fullScreen open={open} onClose={() => setOpen(false)}>
              <AppBar className={classes.appBar}>
                <Toolbar>
                  <Typography variant="h6" className={classes.title}>
                    {props.title}
                  </Typography>
                  <IconButton
                    edge="start"
                    color="inherit"
                    onClick={() => setOpen(false)}
                    aria-label="Close"
                  >
                    <FullScreenExitIcon />
                  </IconButton>
                </Toolbar>
              </AppBar>
            </Dialog>
          </div>
        )}
      </header>
    </div>
  );
};

LaneHeader.propTypes = {
  styles: PropTypes.object, //style ของ Header
  title: PropTypes.string //หัวข้อที่แสดง
};

export default LaneHeader;
