import React from "react";
import { reduxForm, Field } from "redux-form";
import { connect } from "react-redux";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  MenuItem
} from "@material-ui/core";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { TextInput } from "./FormComponent";
// validation
import { createValidator, required } from "./../constants/validation";


// action
import {
  getAllCategory,

} from "./../actions/category.action"


const DialogFormFood = props => {
  const { handleCloseDialog, handleOpenDialog, handleSubmit, getAllCategory, categoryReducer } = props;

  React.useEffect(() => {
    getAllCategory();
  }, [getAllCategory]);


  return (
    <div>
      <Dialog
        open={handleOpenDialog}
        maxWidth="sm"
        fullWidth={true}
        onClose={() => handleCloseDialog()}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">{document.title}</DialogTitle>

        <form onSubmit={handleSubmit} autoComplete="off">
          <DialogContent>
            <Field
              name="food_name"
              component={TextInput}
              label="Food name"
              margin="normal"
              variant="outlined"
              type="text"
              fullWidth
            />

            <Field
              name="food_type_id"
              select
              label="Category"
              component={TextInput}
              margin="normal"
              variant="outlined"
              fullWidth
            >
              <MenuItem value="">None</MenuItem>
              {categoryReducer.payload.map((item, key) =>
                <MenuItem key={key} value={item.food_type_id}>{item.type_name}</MenuItem>
              )}
            </Field>
            <Field
              name="recommend_flag"
              select
              label="Recommend"
              component={TextInput}
              margin="normal"
              variant="outlined"
              fullWidth
            >
              <MenuItem value="">None</MenuItem>
              <MenuItem value={true}>Recommend</MenuItem>
              <MenuItem value={false}>Unrecommend</MenuItem>
            </Field>

            <Field
              name="active_flag"
              select
              label="สถานะ"
              component={TextInput}
              margin="normal"
              variant="outlined"
              fullWidth
            >
              <MenuItem value="">None</MenuItem>
              <MenuItem value={true}>แสดงผล</MenuItem>
              <MenuItem value={false}>ไม่แสดงผล</MenuItem>
            </Field>


            <Button variant="outlined" margin="normal" color="default" >
              Upload
               <CloudUploadIcon />
            </Button>

          </DialogContent>

          <DialogActions>
            <Button type="submit" color="primary">
              {`Submit`}
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};
const mapStateToProps = ({ categoryReducer }) => ({
  categoryReducer
})

const mapDispatchToProps = {
  getAllCategory
}


const validate = values =>
  createValidator({
    food_name: required,
    food_type_id: required,
    recommend_flag: required,
    active_flag: required
  })(values);

export default reduxForm({
  form: "food-form",
  enableReinitialize: true,
  validate,
  destroyOnUnmount: false,

})(connect(
  mapStateToProps,
  mapDispatchToProps)(DialogFormFood));
