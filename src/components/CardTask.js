import React from "react";
import PropTypes from "prop-types";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

const CardTask = props => {
  return (
    <Card>
      <CardContent>{props.food_name}</CardContent>
    </Card>
  );
};
CardTask.propTypes = {
  styles: PropTypes.object //style ของ Header
  //handleSidebarToggle: PropTypes.func //event handler ในการจัดการเมนูด้านข้าง
};
export default CardTask;
