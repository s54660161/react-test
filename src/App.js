import React from "react";
import { blue } from "@material-ui/core/colors";
import "./App.css";

import RouteApp from "./router/Routes";
import BasePage from "./views/BasePage";
import { createBrowserHistory } from "history";

const history = createBrowserHistory();

const App = () => {
  return <RouteApp history={history} />;
};

export default App;
