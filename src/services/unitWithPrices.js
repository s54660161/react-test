

export const createDataUnitWithPrice = formData => `
mutation{
    createUnitWithPrice(data:{
      unit_price:100
      active_flag:true
      food:{
        connect:{
          food_id: "ck0js5npz00nh0762ybz98tea"
        }
      }
      unit:{
         connect:{
            unit_id: "ck0b1dcfl007x0862dbc0t44q"
         } 
      }
    }){
      foodsize_id,
      food{
        food_id,
        food_name
      },
      unit{
        unit_id,
        unit_name,
        active_flag
      },
      unit_price,
      active_flag
    }
  }`;