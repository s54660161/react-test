
export const getCategoryAll = `
  query {
    categories( orderBy: food_type_id_DESC )
       {
        food_type_id,
        type_name,
        active_flag
      }
  }`;

export const createDataCategory = formData => `
mutation {
    createCategory(data: {
        type_name: "${formData.type_name}"
        active_flag: ${formData.active_flag}
    }) {
        food_type_id,
        type_name,
        active_flag
    }
}`;

export const updateDataCategory = (newFromdata, id) => {
    return `mutation{
        updateCategory(
          data:{
            type_name: "${newFromdata.type_name}",
            active_flag: ${newFromdata.active_flag} }
          where:{
            food_type_id: "${id}"
          }
        ){
            food_type_id,
            type_name,
            active_flag
        }
      }`
}

export const deleteDataCategory = id => {
    return `mutation{
        deleteCategory(where:{
            food_type_id: "${id}"
        }
        ){
            food_type_id,
            type_name,
            active_flag
        }
      }`
}