
export const getFoodAll = `
query{
    foods(
      orderBy: food_id_DESC
    ){
      food_id,
      food_name,
      image_path,
      recommend_flag,
      active_flag,
      category{
        food_type_id,
        type_name,
        active_flag
      }
    }
  }`;


export const createDataFood = formData => `
mutation{
    createFood(
      data:{
        food_name:"${formData.food_name}"
        image_path: "iamge/3.png"
        recommend_flag:${formData.recommend_flag}
        active_flag:${formData.active_flag}
        category:{
            connect:{
                food_type_id: "${formData.food_type_id}"
            }
        }
        
      }
    ){
        food_id,
        food_name,
        image_path,
        recommend_flag,
        active_flag,
        category{
          food_type_id,
          type_name,
          active_flag
        }
      }
  }`;


export const deleteDataFood = id => {
  return `mutation{
      deleteFood(where:{
        food_id: "${id}"
        }
        ){
          food_id,
       
        }
      }`
}


export const updateDataFood = (formData, id) => `
mutation{
  updateFood(data:{
    food_name:"${formData.food_name}"
    image_path: "iamge/3.png"
    recommend_flag:${formData.recommend_flag}
    active_flag:${formData.active_flag}
    category:{
      connect:{
        food_type_id: "${formData.food_type_id}"
      }
   }
    
  }
  where:{
    food_id: "${id}"
  }){
        food_id,
        food_name,
        image_path,
        recommend_flag,
        active_flag,
        category{
          food_type_id,
          type_name,
          active_flag
        }
      }
  }`;
