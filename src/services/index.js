import axios from "axios";

export const apiUrlGraphQL = axios.create({
  baseURL: "http://localhost:4466"
});

export const getDataQl = `
  query {
    posts( 
      orderBy: 
        id_DESC
       )
       {
      id
      title
      content
      published
      }
  }
  `;

export const getDataByID = id => `
  query {
    post(
      where:{
        id:mutation {
          createPost(data: {
            title: "i ฟกห"
            content: "หหห"
            published:true
          }) {
            id
            published
          }
        }
      }
    ) {
      id
      title
      content
      published
    }
  }
  `;
export const deleteDataByID = id => `
mutation{deletePost(
  where: {id: "${id}"}
)
  {
      published
  }
}`;

export const createDataFoodItem = formData => `
mutation {
  createPost(data: {
    title: "${formData.title}"
    content: "${formData.content}"
    published:true
  }) {
    id
    title
    content
    published
  }
}`;

export const updateDataFoodItem = (oldFromdata, newFromdata, id) => {

  return `mutation {
    upsertPost(
      where: 
      {id: "${id}"}
      create: {
        id: "${id}"
        title: "${oldFromdata.title}"
        content:"${oldFromdata.content}"
        published:true
      }
      update:{
        title: "${newFromdata.title}"
        content: "${newFromdata.content}"
        published:true
      }
    )
      {
        id
        title
        content
        published
      }
  }`;
};
