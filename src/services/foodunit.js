
export const getFoodUnitAll = `
  query {
    foodUnits( orderBy: unit_id_DESC )
       {
        unit_id,
        unit_name,
        active_flag
      }
  }`;

export const createDataFoodUnit = formData => `
mutation {
    createFoodUnit(data: {
        unit_name: "${formData.unit_name}"
        active_flag: ${formData.active_flag}
    }) {
        unit_id,
        unit_name,
        active_flag
    }
}`;

export const updateDataFoodUnit = (newFromdata, id) => {
    return `mutation{
        updateFoodUnit(
          data:{
          unit_name: "${newFromdata.unit_name}",
          active_flag: ${newFromdata.active_flag} }
          where:{
                unit_id: "${id}"
          }
        ){
          unit_id,
          unit_name,
          active_flag
        }
      }`
}

export const deleteDataFoodUnit = id => {
    return `mutation{::ซ
        deleteFoodUnit(where:{
          unit_id: "${id}"
        }
        ){
            unit_id,
            unit_name,
            active_flag
        }
      }`
}