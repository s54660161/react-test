import {
  HTTP_LOGIN_FETCHING,
  HTTP_LOGIN_SUCCESS,
  HTTP_LOGIN_FAILED,
  HTTP_LOGOUT,
  server,
  YES,
  ALERT_MESSAGE_SHOW
} from "../constants";

import { apiUrlGraphQL } from "../services";

export const setLoginStateToFetching = () => ({
  type: HTTP_LOGIN_FETCHING
});
export const setLoginStateToSuccess = payload => ({
  type: HTTP_LOGIN_SUCCESS,
  payload
});

export const setLoginStateToFailed = error => ({
  type: HTTP_LOGIN_FAILED,
  payload: error
});

export const logout = () => {
  return (dispatch, getState) => {
    try {
      //console.log("actione5e5eee");
      dispatch({ type: HTTP_LOGOUT.FETCHING });
      localStorage.removeItem(server.LOGIN_PASSED);
      dispatch({ type: HTTP_LOGOUT.SUCCESS });
    } catch (err) {
      console.log({ err });
      dispatch({ type: HTTP_LOGOUT.FAILURE, payload: err });
    }
  };
};

const GET_ORGANIZATION = `
query {
  posts {
    id
    title
    content
    published
  }
}
`;
export const login = (history, credentail) => {
  return async (dispatch, getState) => {
    try {
      //dispatch(setLoginStateToFetching());
      const result = await apiUrlGraphQL.post("", {
        query: GET_ORGANIZATION
      });
      //console.log("result", result.data.data);
      localStorage.setItem(server.LOGIN_PASSED, YES);
      dispatch(setLoginStateToSuccess(result.data.data));
    } catch (error) {
      dispatch({
        type: ALERT_MESSAGE_SHOW,
        payload: error
      });
    }

    // dispatch(setLoginStateToFetching());
    // let result = await httpClient.post(server.LOGIN_URL, credentail);
    // if (result.data.result === OK) {
    //   localStorage.setItem(server.LOGIN_PASSED, YES);
    //   //console.log(getState());
    //   //getState().appReducer.app.forceUpdate();

    //   //history.push("/");
    //   dispatch(setLoginStateToSuccess(result.data.result));
    // } else {
    //   dispatch(setLoginStateToFailed());
    // }
  };
};
