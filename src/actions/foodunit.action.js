import { HTTP_FOODUNIT } from "../constants";
// api path
import {
    apiUrlGraphQL
} from "../services";

// GraphQL
import {
    getFoodUnitAll,
    createDataFoodUnit,
    updateDataFoodUnit,
    deleteDataFoodUnit
} from "../services/foodunit";


export const getFoodUnitToFetching = payload => ({
    type: HTTP_FOODUNIT.FETCHING
});
export const getFoodUnitToSuccess = (payload) => ({
    type: HTTP_FOODUNIT.SUCCESS,
    payload
})

export const getFoodUnitToFailure = error => ({
    type: HTTP_FOODUNIT.FAILURE,
    payload: error
});



export const getAllFoodUnit = () => {
    return async dispatch => {
        try {
            dispatch(getFoodUnitToFetching());
            const result = await apiUrlGraphQL.post("", { query: getFoodUnitAll });
            console.log(result)
            dispatch(getFoodUnitToSuccess(result.data.data.foodUnits));
        } catch (error) {
            dispatch(getFoodUnitToFailure(error));
        }
    };
};

export const createFoodUnit = formData => {
    return async dispatch => {
        try {
            console.log(formData);
            dispatch(getFoodUnitToFetching())
            const result = await apiUrlGraphQL.post("", {
                query: createDataFoodUnit(formData)
            });
            //console.log(result);

            dispatch(getAllFoodUnit());
        } catch (error) {
            dispatch(getFoodUnitToFailure(error));
            console.log(error);
        }
    };
};

export const updateFoodUnit = (formData, id) => {
    return async dispatch => {
        try {
            dispatch(getFoodUnitToFetching())
            const result = await apiUrlGraphQL.post("", {
                query: updateDataFoodUnit(formData, id)
            })
            dispatch(getAllFoodUnit())
        } catch (error) {
            dispatch(getFoodUnitToFailure(error))
        }
    }
}

export const deleteFoodUnit = id => {
    return async dispatch => {
        try {
            dispatch(getFoodUnitToFetching())
            const result = await apiUrlGraphQL.post("", {
                query: deleteDataFoodUnit(id)
            })
            dispatch(getAllFoodUnit())
        } catch (error) {
            dispatch(getFoodUnitToFailure(error))
        }
    }
}
