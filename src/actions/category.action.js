import { HTTP_CATEGORY } from "../constants";
// api path
import {
    apiUrlGraphQL
} from "../services";

// GraphQL
import {
    getCategoryAll,
    createDataCategory,
    updateDataCategory,
    deleteDataCategory
} from "../services/category";


export const getCategoryToFetching = payload => ({
    type: HTTP_CATEGORY.FETCHING
});
export const getCategoryToSuccess = (payload) => ({
    type: HTTP_CATEGORY.SUCCESS,
    payload
})

export const getCategoryToFailure = error => ({
    type: HTTP_CATEGORY.FAILURE,
    payload: error
});



export const getAllCategory = () => {
    return async dispatch => {
        try {
            dispatch(getCategoryToFetching());
            const result = await apiUrlGraphQL.post("", { query: getCategoryAll });
            console.log(result)
            dispatch(getCategoryToSuccess(result.data.data.categories));
        } catch (error) {
            dispatch(getCategoryToFailure(error));
        }
    };
};

export const createCategory = formData => {
    return async dispatch => {
        try {
            console.log(formData);
            dispatch(getCategoryToFetching())
            const result = await apiUrlGraphQL.post("", {
                query: createDataCategory(formData)
            });
            //console.log(result);

            dispatch(getAllCategory());
        } catch (error) {
            dispatch(getCategoryToFailure(error));
            console.log(error);
        }
    };
};

export const updateCategory = (formData, id) => {
    return async dispatch => {
        try {
            dispatch(getCategoryToFetching())
            const result = await apiUrlGraphQL.post("", {
                query: updateDataCategory(formData, id)
            })
            dispatch(getAllCategory())
        } catch (error) {
            dispatch(getCategoryToFailure(error))
        }
    }
}

export const deleteCategory = id => {
    return async dispatch => {
        try {
            dispatch(getCategoryToFetching())
            const result = await apiUrlGraphQL.post("", {
                query: deleteDataCategory(id)
            })
            dispatch(getAllCategory())
        } catch (error) {
            dispatch(getCategoryToFailure(error))
        }
    }
}


