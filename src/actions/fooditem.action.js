import { HTTP_FOOD } from '../constants';
import { apiUrlGraphQL } from '../services';

import {
  getFoodAll,
  createDataFood,
  deleteDataFood,
  updateDataFood,
} from '../services/food';

export const getFoodItemToFetching = (payload) => ({
  type: HTTP_FOOD.FETCHING,
});
export const getFoodItemToSuccess = (payload, isStatus) => ({
  type: HTTP_FOOD.SUCCESS,
  payload,
  isStatus,
});

export const getFoodItemToFailure = (error) => ({
  type: HTTP_FOOD.FAILURE,
  payload: error,
});

export const getAllFoodItem = () => {
  return async (dispatch) => {
    try {
      dispatch(getFoodItemToFetching());
      const result = await apiUrlGraphQL.post('', { query: getFoodAll });

      //console.log({ result })
      dispatch(getFoodItemToSuccess(result.data.data.foods));
    } catch (error) {
      dispatch(getFoodItemToFailure(error));
    }
  };
};

export const deleteFoodItemById = (id) => {
  return async (dispatch) => {
    try {
      dispatch(getFoodItemToFetching());
      console.log(id);
      const result = await apiUrlGraphQL.post('', {
        query: deleteDataFood(id),
      });
      if (result.data.data != null) {
        dispatch(getAllFoodItem());
      } else {
        dispatch(getFoodItemToFailure(result.data.errors[0].message));
      }
    } catch (error) {
      dispatch(getFoodItemToFailure(error));
    }
  };
};

export const createFoodItem = (formData) => {
  return async (dispatch) => {
    try {
      dispatch(getFoodItemToFetching());
      await apiUrlGraphQL.post('', {
        query: createDataFood(formData),
      });
      dispatch(getAllFoodItem());
    } catch (error) {
      dispatch(getFoodItemToFailure(error));
      console.log(error);
    }
  };
};

export const updateFoodItem = (formData, id) => {
  //console.log(formData);
  return async (dispatch) => {
    try {
      dispatch(getFoodItemToFetching());
      await apiUrlGraphQL.post('', {
        query: updateDataFood(formData, id),
      });

      dispatch(getAllFoodItem());
    } catch (error) {
      dispatch(getFoodItemToFailure(error));
      console.log(error);
    }
  };
};
