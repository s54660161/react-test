import { server, YES } from "../constants";

export const autoLogin = history => {
  return () => {
    if (localStorage.getItem(server.LOGIN_PASSED) === YES) {
      setTimeout(() => history.push("/"), 100);
    }
  };
};
