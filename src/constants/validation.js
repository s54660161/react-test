import moment from "moment";

export const isEmpty = value =>
  value === undefined ||
  value === null ||
  value === "" ||
  (typeof value === "object" && Object.keys(value).length === 0);
export const join = rules => (value, data) =>
  rules.map(rule => rule(value, data)).filter(error => !!error)[0];

export const email = value => {
  // Let's not start a debate on email regex. This is just for an example app!
  if (
    !isEmpty(value) &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
  ) {
    return "Invalid E-mail.";
  }
};

export const required = value => {
  if (isEmpty(value)) {
    return "Required.";
  }
};

export const customRequired = label => value => {
  if (isEmpty(value)) {
    return `${label} Required.`;
  }
};

export const customFullRequired = label => value => {
  if (isEmpty(value)) {
    return `${label}`;
  }
};

// export const addressRequired = (label, country, value1, value2, value3) => (value) => {
//   if (country === PROJECT_UK) {
//     if (isEmpty(value1) && isEmpty(value2) && isEmpty(value3)) {
//       return `${label}`
//     }
//   } else if (isEmpty(value1) && isEmpty(value2)) {
//     return `${label}`
//   }
// }

export const minLength = min => value => {
  if (!isEmpty(value) && value.length < min) {
    return `Min less ${min} characters.`;
  }
};

export const maxLength = max => value => {
  if (!isEmpty(value) && value.length > max) {
    return `Must more ${max} characters.`;
  }
};

export const integer = value => {
  if (!Number.isInteger(Number(value))) {
    return "Must integer.";
  }
};

export const oneOf = enumeration => value => {
  if (!~enumeration.indexOf(value)) {
    return `Must one of: ${enumeration.join(", ")}`;
  }
};

export const match = field => (value, data) => {
  if (data) {
    if (value !== data[field]) {
      return "Do not match.";
    }
  }
};

export const number = value => {
  if (!/^[0-9]*$/.test(value)) {
    return "Must number.";
  }
};

export const customNumber = label => value => {
  if (!/^[0-9]*$/.test(value)) {
    return label;
  }
};

export const lower = value => {
  if (!/^[a-z]*$/.test(value)) {
    return "Must lower charecters.";
  }
};

export const upper = value => {
  if (!/^[A-Z]*$/.test(value)) {
    return "Must upper charecters.";
  }
};

export const haveSpecialCharacter = value => {
  if (/[\[\]\^\$\|\?*\+()\~`!\/@#%&฿_+={}'""<>:;,]/.test(value)) {
    return "Must no special charecters.";
  }
};

export const password = value => {
  if (
    !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d){8,}.+$/.test(value) ||
    value.length < 8
  ) {
    return "Your password must have:\n• 8 or more characters\n• At least one number\n• Upper & lowercase letters";
  }
};

export const compare = (value1, value2) => value => {
  if (value1 !== value2 || !value) {
    return "Your password doesn’t match.";
  }
};

export const dateRange = (startDateTime, endDateTime) => value => {
  const dateIsBefore = moment(startDateTime).isBefore(moment(endDateTime));
  if (!dateIsBefore || !value) {
    return "End date time must more start date time.";
  }
};

export const createValidator = rules => (data = {}) => {
  const errors = {};
  Object.keys(rules).forEach(key => {
    const rule = join([].concat(rules[key]));
    const error = rule(data[key], data);
    if (error) {
      errors[key] = error;
    }
  });
  return errors;
};
