import axios from "axios";

const FETCHING = "FETCHING";
const SUCCESS = "SUCCESS";
const FAILURE = "FAILURE";

function createRequestTypes(base) {
  return [FETCHING, SUCCESS, FAILURE].reduce((acc, type) => {
    acc[type] = `${base}_${type}`;
    return acc;
  }, {});
}
// Login Page
export const APP_INIT = "APP_INIT";

// Login Page
export const HTTP_LOGIN = createRequestTypes("HTTP_LOGIN");
export const HTTP_LOGIN_FETCHING = "HTTP_LOGIN_FETCHING";
export const HTTP_LOGIN_SUCCESS = "HTTP_LOGIN_SUCCESS";
export const HTTP_LOGIN_FAILED = "HTTP_LOGIN_FAILED";

//logout
export const HTTP_LOGOUT = createRequestTypes("HTTP_LOGOUT");

// Register Page
export const HTTP_REGISTER_FETCHING = "HTTP_REGISTER_FETCHING";
export const HTTP_REGISTER_SUCCESS = "HTTP_REGISTER_SUCCESS";
export const HTTP_REGISTER_FAILED = "HTTP_REGISTER_FAILED";


//ALERT
export const ALERT_MESSAGE_SHOW = "ALERT_MESSAGE_SHOW";
export const ALERT_MESSAGE_RESET = "ALERT_MESSAGE_RESET";

//FOOD ITEM
export const HTTP_FOOD = createRequestTypes("HTTP_FOOD");

//CATEGORY
export const HTTP_CATEGORY = createRequestTypes("HTTP_CATEGORY");

//FOOD UNIT
export const HTTP_FOODUNIT = createRequestTypes("HTTP_FOODUNIT");

export const YES = "YES";
export const NO = "NO";
export const OK = "ok";
export const NOK = "nok";

export const server = {
  LOGIN_URL: `authen/login`,
  REGISTER_URL: `authen/register`,
  PRODUCT_URL: `stock/product`,
  TRANSACTION_URL: `transaction`,
  REPORT_URL: `stock/report`,
  LOGIN_PASSED: `yes`
};
