import React, { Component } from "react";
import { Router, Route, Switch } from "react-router-dom";

import HomeIcon from "@material-ui/icons/Home";
import FoodIcon from "@material-ui/icons/LocalDining";
import QueueIcon from "@material-ui/icons/FormatListBulleted";
import Category from "@material-ui/icons/Category";
import GrainIcon from '@material-ui/icons/Grain';

import HomePage from "./../views/HomePage";
import FoodItemPage from "./../views/FoodItemPage";
import CategoryItemPage from "./../views/CategoryFood";
import FoodUnitPage from "./../views/FoodUnit";
import QueuePage from "./../views/QueuePage";
import LoginPage from "./../views/LoginPage";
import SecuredRoute from "./SecuredRoute";

export const Routes = [
  {
    title: "หน้าหลัก", //ชื่อเมนู
    path: "/", //route path ที่แสดงอยู่บน url
    sidebarName: "Home",
    navbarName: "Home",
    icon: <HomeIcon />, //icon แสดงอยู่หน้า title
    component: props => <SecuredRoute component={HomePage} props={props} /> //หน้าจอ ที่แสดง
  },
  {
    path: "/queues",
    sidebarName: "Queue",
    navbarName: "Queue",
    icon: <QueueIcon />,
    component: props => <SecuredRoute component={QueuePage} props={props} />
  },
  {
    path: "/fooditems",
    sidebarName: "Food",
    navbarName: "Food",
    icon: <FoodIcon />,
    component: props => <SecuredRoute component={FoodItemPage} props={props} />
  },
  {
    path: "/categoryitems",
    sidebarName: "Category",
    navbarName: "Category",
    icon: <Category />,
    component: props => (
      <SecuredRoute component={CategoryItemPage} props={props} />
    )
  },
  {
    path: "/foodunit",
    sidebarName: "FoodUnit",
    navbarName: "FoodUnit",
    icon: <GrainIcon />,
    component: props => (
      <SecuredRoute component={FoodUnitPage} props={props} />
    )
  }
];

class RouteApp extends Component {
  render() {
    const { history } = this.props;
    //console.log("route", this.props);
    return (
      <Router history={history}>
        <Switch>
          {Routes.map(route => (
            <Route
              path={route.path}
              component={route.component}
              key={route.path}
              exact={true}
            />
          ))}
          {
            <Route
              path="/login"
              component={LoginPage}
              history={history}
              exact={true}
            />
          }
        </Switch>
      </Router>
    );
  }
}

export default RouteApp;
