import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import BasePage from "./../views/BasePage";

class SecuredRoute extends Component {
  render() {
    //console.log(this.props);
    const { props, authReducer, component: Component } = this.props;
    return (
      <div>
        {authReducer.isLogin ? (
          <BasePage component={Component} props={props} />
        ) : (
          // <Redirect to="/login" />
          <BasePage component={Component} props={props} />
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ authReducer }) => ({ authReducer });

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  null
)(SecuredRoute);
