import { createMuiTheme } from '@material-ui/core/styles';
import {purple,green,blue, grey} from '@material-ui/core/colors';
//import green from '@material-ui/core/colors/green';

const theme = createMuiTheme({
    pallette:{
        primary: purple,
        secondary: green,
    },
    appBar: {
        height: 57,
        color: blue
    },
    drawer:{
        width: 230,
        color: grey
    },
    status:{
        danger: 'orange',
    }
});

export default theme
