import { ALERT_MESSAGE_SHOW, ALERT_MESSAGE_RESET } from "../constants";

const initialState = {
  isFetching: false,
  isError: false,
  isMessage: ""
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ALERT_MESSAGE_SHOW:
      return {
        ...state,
        isFetching: false,
        isError: true,
        isMessage: payload
      };
    case ALERT_MESSAGE_RESET:
      return {
        ...state,
        isFetching: false,
        isError: false,
        isMessage: ""
      };
    default:
      return state;
  }
};
