import {
  HTTP_LOGIN_FETCHING,
  HTTP_LOGIN_SUCCESS,
  HTTP_LOGIN_FAILED,
  HTTP_LOGOUT
} from "../constants";

const initialState = {
  result: null,
  error: {},
  isFetching: false,
  isError: false,
  isLogin: false
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case HTTP_LOGIN_FETCHING:
      return {
        ...state,
        isFetching: true
      };
    case HTTP_LOGIN_SUCCESS:
      return {
        ...state,
        result: payload,
        isFetching: false,
        isError: false,
        isLogin: true
      };
    case HTTP_LOGIN_FAILED:
      return {
        ...state,
        result: null,
        isFetching: false,
        isError: true,
        error: payload,
        isLogin: false
      };

    // Case for logout
    case HTTP_LOGOUT.FETCHING:
      return {
        ...state,
        isFetching: true
      };
    case HTTP_LOGOUT.SUCCESS:
      return {
        ...state,
        result: payload,
        isFetching: false,
        isError: false,
        isLogin: false
      };
    case HTTP_LOGOUT.FAILURE:
      return {
        ...state,
        result: payload,
        isFetching: false,
        isError: false,
        error: payload,
        isLogin: false
      };
    default:
      return state;
  }
};
