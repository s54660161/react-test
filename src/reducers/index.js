import { combineReducers } from "../../node_modules/redux";

//Reducer js
import authReducer from "./login.reducer";
import alertReducer from "./alert.reducer";
import foodItemReducer from "./fooditem.reducer";
import categoryReducer from "./category.reducer";
import foodUnitReducer from "./foodunit.reducer";
import appReducer from "./app.reducer";


import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { reducer as form } from "redux-form";

const loginPersist = {
  key: "authReducer",
  storage
};

export default combineReducers({
  form,
  authReducer: persistReducer(loginPersist, authReducer),
  appReducer,
  alertReducer,
  foodItemReducer,
  categoryReducer,
  foodUnitReducer
});
