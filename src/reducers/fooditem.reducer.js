import { HTTP_FOOD } from "../constants";
const initialState = {
  isFetching: false,
  payload: [],
  isError: false,
  // isDelete: false,
  // isEdit: false,
  // isStatus: false,
  error: {}
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case HTTP_FOOD.FETCHING:

      return {
        ...state,
        isFetching: true,
        payload: [],
        isError: false,
        error: {}
      };
    case HTTP_FOOD.SUCCESS:

      return {
        ...state,
        isFetching: false,
        payload: payload,
        isError: false,
        // isStatus: isStatus != undefined ? true : false,
        // isDelete: _.has(isStatus, 'isDelete'),
        // isEdit: _.has(isStatus, 'isEdit'),
        error: {}
      };
    case HTTP_FOOD.FAILURE:
      return {
        ...state,
        isFetching: false,
        payload: [],
        isError: true,
        error: payload
      };
    default:
      return state;
  }
};
