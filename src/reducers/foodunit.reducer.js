import { HTTP_FOODUNIT } from "../constants";
const initialState = {
    isFetching: false,
    payload: [],
    isError: false,
    error: {}
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case HTTP_FOODUNIT.FETCHING:
            return {
                ...state,
                isFetching: true,
                payload: [],
                isError: false,
                error: {}
            };
        case HTTP_FOODUNIT.SUCCESS:
            return {
                ...state,
                isFetching: false,
                payload: payload,
                isError: false,
                error: {}
            };
        case HTTP_FOODUNIT.FAILURE:
            return {
                ...state,
                isFetching: false,
                payload: [],
                isError: true,
                error: payload
            };
        default:
            return state;
    }
};
