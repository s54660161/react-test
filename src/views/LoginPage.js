import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { connect, useSelector } from "react-redux";
import { login } from "./../actions/login.action";

import AlertMessage from "./../components/AlertMessage";

import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import ThemeDefault from "../theme/default-theme";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(10)
  },
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  paper: {
    padding: theme.spacing(6),
    margin: "auto",
    maxWidth: 500
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  textLogin: {
    textAlign: "center"
  },
  button: {
    margin: theme.spacing(1)
  }
}));

const LoginPage = props => {
  const { login, history, authReducer, alertReducer } = props;
  const classes = useStyles();
  console.log(login);
  // State in login
  const [username, setUsername] = React.useState({
    username: ""
  });

  const [password, setPassword] = React.useState({
    password: ""
  });

  //const isLoginState = useSelector(state => state.authReducer.isLogin);
  if (authReducer.isLogin) {
    history.push("/");
  }

  return (
    <ThemeProvider theme={ThemeDefault}>
      <div className={classes.root} />
      <Paper className={classes.paper}>
        <h1 className={classes.textLogin}>Login Page</h1>
        <form className={classes.container} noValidate autoComplete="off">
          <TextField
            required={true}
            id="outlined-name"
            label="Username"
            type="email"
            className={classes.textField}
            onChange={e => {
              e.preventDefault();
              e.persist();
              setUsername(e.target.value);
            }}
            margin="normal"
            fullWidth
            variant="outlined"
          />
          <TextField
            id="outlined-password"
            type="password"
            label="Password"
            className={classes.textField}
            onChange={e => {
              e.preventDefault();

              setPassword(e.target.value);
            }}
            margin="normal"
            fullWidth
            variant="outlined"
          />
          {authReducer.isError && "Error"}
          <Button
            size="large"
            variant="contained"
            color="primary"
            onClick={e => {
              login(history, { username, password });
            }}
            className={classes.button}
          >
            Login
          </Button>
        </form>
      </Paper>
      {alertReducer.isError ? (
        <AlertMessage
          message={alertReducer.isMessage}
          variant={alertReducer.isError}
        />
      ) : (
        ""
      )}
    </ThemeProvider>
  );
};

LoginPage.propTypes = {
  onAuthenticated: PropTypes.func
};

const mapStateToProps = ({ authReducer, alertReducer }) => ({
  authReducer,
  alertReducer
});

const mapDispatchToProps = {
  login
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
