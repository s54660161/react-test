/**
 * Description: หน้าจอหลักของโปรแกรม
 */
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

//material
import clsx from "clsx";
import CssBaseline from "@material-ui/core/CssBaseline";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import Container from "@material-ui/core/Container";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Menu from "@material-ui/core/Menu";
import Typography from "@material-ui/core/Typography";
import { List, ListItem, ListItemText } from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";

//เรียกใช้งาน icon
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

import { Routes } from "./../router/Routes";
import DefaultTheme from "../theme/default-theme";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { log } from "util";

// action auth
import { logout } from "./../actions/login.action";

const sidebarWidth = 240; //กำหนดความกว้างของ menu sidebar

const BasePage = props => {
  const useStyles = makeStyles(theme => ({
    root: {
      display: "flex"
    },
    appBar: {
      //กำหนด style ของ AppBar
      zIndex: theme.zIndex.drawer + 1, //กำหนดให้ z-index อยู่เหนือ css ของ sidebar เสมอ
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    appBarShift: {
      //กำหนด style ของ AppBar เมื่อ Drawer เปืดอยู่
      marginLeft: sidebarWidth,
      width: `calc(100% - ${sidebarWidth}px)`, //สร้างพื้อที่ว่างให้กับ Drawer
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    container: {
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4)
    },
    content: {
      //style ของส่วนเนื้อหา
      flexGrow: 1,
      height: "100vh",
      overflow: "auto"
    },
    menuButton: {
      //style ปุ่มเมนูในกรณีที่เปิด
      marginLeft: 5,
      marginRight: 36
      //marginRight: theme.spacing(2),
    },
    menuButtonHidden: {
      //style ปุ่มเมนูกรณที่ปิด
      display: "none"
    },
    sidebar: {
      width: sidebarWidth,
      flexShrink: 0,
      whiteSpace: "nowrap"
    },
    sidebarShow: {
      //style กรณี sidebar เปิด
      position: "relative",
      whiteSpace: "nowrap",
      width: sidebarWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    sidebarHidden: {
      //style กรณี sidebar ปิด
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1
      }
    },
    title: {
      flexGrow: 1
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: "0 8px",
      ...theme.mixins.toolbar
    },
    tookbarIcon: {
      display: "flex"
    }
  }));

  function handleDrawerOpen() {
    setIsSidebarOpen(true);
  }

  function handleDrawerClose() {
    setIsSidebarOpen(false);
  }
  const {
    component: Component,
    props: propsData,
    logout,
    statusSidebar
  } = props;
  // console.log(props);
  const classes = useStyles();
  const theme = useTheme();
  const [isSidebarOpen, setIsSidebarOpen] = useState(false); //สถานะแสดงว่า sidebar เปิดอยู่หรืิอไม่
  const [anchorUserMenu, setAnchorUserMenu] = useState(null); //element ที่อ้างถึงในเมนู user

  return (
    <React.Fragment>
      <div className={classes.root}>
        <ThemeProvider theme={DefaultTheme}>
          {/**เคลียร์ค่า default ของ css ที่แตกต่างกันในแต่ละ browser */}
          <CssBaseline />
          {/** ส่วนหัวของหน้าจอ AppBar*/}
          <AppBar
            position="fixed"
            className={clsx(classes.appBar, {
              [classes.appBarShift]: isSidebarOpen
            })}
          >
            <Toolbar className={classes.toolbar}>
              <IconButton
                edge="start"
                color="inherit"
                className={clsx(classes.menuButton, {
                  [classes.menuButtonHidden]: isSidebarOpen
                })}
                onClick={handleDrawerOpen}
                aria-label="แสดงเมนู"
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                ร้านข้าวต้มอนันต์
              </Typography>
              <div>
                <IconButton
                  aria-owns={Boolean(anchorUserMenu) ? "menu-user" : undefined}
                  aria-haspopup="true"
                  onClick={event => setAnchorUserMenu(event.currentTarget)}
                >
                  <AccountCircleIcon />
                </IconButton>
                <Menu
                  id="menu-user"
                  anchorEl={anchorUserMenu}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right"
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right"
                  }}
                  open={Boolean(anchorUserMenu)}
                  onClose={() => setAnchorUserMenu(null)}
                >
                  <MenuItem onClick={() => setAnchorUserMenu(null)}>
                    ข้อมูลผู้ใช้งาน
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      setAnchorUserMenu(null);
                      logout();
                    }}
                  >
                    ออกจากระบบ
                  </MenuItem>
                </Menu>
              </div>
            </Toolbar>
          </AppBar>
          {/** sidebar เมนู */}
          <Drawer
            variant="permanent"
            className={clsx(classes.sidebar, {
              [classes.sidebarShow]: isSidebarOpen,
              [classes.sidebarHidden]: !isSidebarOpen
            })}
            classes={{
              paper: clsx({
                [classes.sidebarShow]: isSidebarOpen,
                [classes.sidebarHidden]: !isSidebarOpen
              })
            }}
            open={isSidebarOpen}
          >
            <div className={classes.toolbar}>
              <IconButton onClick={handleDrawerClose}>
                {theme.direction === "rtl" ? (
                  <ChevronRightIcon />
                ) : (
                    <ChevronLeftIcon />
                  )}
              </IconButton>
            </div>
            <Divider />
            <List>
              {Routes.map((text, index) => (
                <Link to={text.path} key={index}>
                  <ListItem button>
                    <ListItemIcon>{text.icon}</ListItemIcon>
                    <ListItemText primary={text.sidebarName} />
                  </ListItem>
                </Link>
              ))}
            </List>
          </Drawer>
          <main id="Content" className={classes.content}>
            <Container maxWidth="lg" className={classes.container}>
              <div className={classes.toolbar} />
              {/*<Switch>
                        <Route exact path="/" component={HomePage}/>
                        <Route path="/queue" component={QueuePage} />
                    </Switch>*/}
              {/* ส่วนของ Content กลาง */}
              <Component {...propsData} />
            </Container>
          </main>
        </ThemeProvider>
      </div>
    </React.Fragment>
  );
};
const mapStateToProps = state => ({});

const mapDispatchToProps = {
  logout
};

//export default withWidth()(BasePage);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BasePage);
