import React from "react";
import Typography from "@material-ui/core/Typography";


const HomePage = () => {
  React.useEffect(() => {
    document.title = "หน้าหลัก";
  }, []);

  return (
    <React.Fragment>
      Home Page
    </React.Fragment>
  );
};

export default HomePage;
