import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import MaterialTable from "material-table";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import DialogFormFood from "./../components/DialogFormFood";
import LinearProgress from "@material-ui/core/LinearProgress";

// action
import {
  getAllFoodItem,
  deleteFoodItemById,
  createFoodItem
} from "./../actions/fooditem.action";

const FoodItemPage = props => {
  const {
    getAllFoodItem,
    foodItemReducer,
    deleteFoodItemById,
    createFoodItem
  } = props;
  //const [step, setStep] = React.useState(foodItemReducer.payload.length);

  React.useEffect(() => {
    document.title = "จัดการรายการอาหาร";

    getAllFoodItem();
  }, []);

  const [state, setState] = React.useState({
    columns: [
      { title: "ID", field: "id" },
      { title: "Titile", field: "title" },
      { title: "Content", field: "content" }
    ]
  });
  const [showDialog, setShowDialog] = React.useState(false);
  const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1
    },
    button: {
      margin: theme.spacing(1)
    },
    input: {
      display: "none"
    }
  }));

  const classes = useStyles();

  function openDialogForm() {
    setShowDialog(false);
  }
  const submitFormFood = values => {
    createFoodItem(values);
    setShowDialog(false);
  };

  return (
    <React.Fragment>
      {foodItemReducer.isFetching ? (
        <div className={classes.root}>
          <LinearProgress />
        </div>
      ) : (
        <div>
          <DialogFormFood
            onSubmit={submitFormFood}
            handleOpenDialog={showDialog}
            handleCloseDialog={() => openDialogForm()}
          />
          <Button
            onClick={() => {
              setShowDialog(true);
            }}
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Primary
          </Button>
          <MaterialTable
            title={document.title}
            columns={state.columns}
            data={foodItemReducer.payload}
            options={{
              pageSize: 20,
              pageSizeOptions: [20, 25, 30, 35]
            }}
            editable={{
              onRowAdd: newData =>
                new Promise(resolve => {
                  setTimeout(() => {
                    resolve();
                    createFoodItem(newData);
                    // console.log(newData);
                    // const data = [...state.data];
                    // data.push(newData);
                    // setState({ ...state, data });
                  }, 600);
                }),
              onRowUpdate: (newData, oldData) =>
                new Promise(resolve => {
                  setTimeout(() => {
                    resolve();
                    const data = [...state.data];
                    data[data.indexOf(oldData)] = newData;
                    setState({ ...state, data });
                  }, 600);
                }),
              onRowDelete: oldData =>
                new Promise(resolve => {
                  setTimeout(() => {
                    resolve();
                    deleteFoodItemById(oldData.id);
                    // setStep(oldData.id);
                  }, 600);
                })
            }}
          />
        </div>
      )}
    </React.Fragment>
  );
};
const mapStateToProps = ({ foodItemReducer }) => ({ foodItemReducer });

const mapDispatchToProps = {
  getAllFoodItem,
  deleteFoodItemById,
  createFoodItem
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FoodItemPage);
