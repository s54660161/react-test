import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import swal from "sweetalert";


import FormDialogFoodUnit from "./../components/DialogFormFoodUnit";

//material-ui
import { lighten, makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel
} from "@material-ui/core";

import {
  Toolbar,
  Typography,
  Paper,
  IconButton,
  Tooltip,
  LinearProgress,
  InputAdornment,
  TextField
} from "@material-ui/core";
import { Delete, Search, AddCircle, Build } from "@material-ui/icons";

// action
import {
  getAllFoodUnit,
  createFoodUnit,
  updateFoodUnit,
  deleteFoodUnit
} from "./../actions/foodunit.action"

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const headRows = [
  {
    id: "unit_id",
    numeric: false,
    disablePadding: true,
    label: "ID"
  },
  { id: "unit_name", numeric: true, disablePadding: false, label: "Name" },
  { id: "active_flag", numeric: true, disablePadding: false, label: "Status" }
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;

  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headRows.map(row => (
          <TableCell
            key={row.id}
            align={row.numeric ? "right" : "left"}
            padding="default"
            sortDirection={orderBy === row.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === row.id}
              direction={order}
              onClick={createSortHandler(row.id)}
            >
              {row.label}
              {orderBy === row.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="right">Manage</TableCell>
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1)
  },
  highlight:
    theme.palette.type === "light"
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85)
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark
      },
  spacer: {
    flex: "1 1 50%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  },
  margin: {
    margin: theme.spacing(1)
  }
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { openDialogForm, onChangeSearch } = props;

  return (
    <Toolbar>
      <div className={classes.title}>
        <Typography variant="h6" id="tableTitle">
          {document.title}
        </Typography>
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        <div>
          <Tooltip title="Filter list">
            <TextField
              className={classes.margin}
              id="input-with-icon-textfield"
              onChange={e => {
                //console.log(e.target.value);
                e.persist();
                onChangeSearch(e.target.value);
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search />
                  </InputAdornment>
                )
              }}
            />
          </Tooltip>

          <Tooltip title="Add">
            <IconButton
              aria-label="Add"
              onClick={e => {
                openDialogForm(e, false);
              }}
            >
              <AddCircle />
            </IconButton>
          </Tooltip>
        </div>
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {};

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3)
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2)
  },
  table: {
    minWidth: 750
  },
  tableWrapper: {
    overflowX: "auto"
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1
  }
}));

const CategoryFood = props => {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("calories");
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(20);
  const [showDialog, setShowDialog] = React.useState(false);
  const [initialValues, setInitialValues] = React.useState(null);

  const {
    foodUnitReducer,
    getAllFoodUnit,
    createFoodUnit,
    updateFoodUnit,
    deleteFoodUnit
  } = props;

  React.useEffect(() => {
    document.title = "จัดการรายการหน่วยของอาหาร";
    getAllFoodUnit();
  }, [getAllFoodUnit]);


  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === "desc";
    setOrder(isDesc ? "asc" : "desc");
    setOrderBy(property);
  }

  function handleChangePage(event, newPage) {
    setPage(newPage);
  }

  function handleChangeRowsPerPage(event) {
    setRowsPerPage(+event.target.value);
    setPage(0);
  }
  const emptyRows =
    rowsPerPage -
    Math.min(rowsPerPage, foodUnitReducer.payload.length - page * rowsPerPage);

  function openDialogForm(e, row, isEdit) {
    e.preventDefault();
    const initailIsEdit = isEdit ? row : {};
    setInitialValues(initailIsEdit);
    setShowDialog(true);
  }

  function closeDialogForm(e) {

    e.preventDefault();

    setInitialValues(null);
    setShowDialog(false);
  }

  function deleteDataRows(id) {
    swal({
      title: "Are you sure?",
      text:
        "Once deleted, you will not be able to recover this imaginary file!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
         deleteFoodUnit(id);
        // console.log("foodUnitReducer", foodUnitReducer);
        // if (foodUnitReducer.error != "") {
        //   swal("Poof! Your imaginary file has been deleted!", {
        //     icon: "success"
        //   });
        // }
      }
    });
  }

  const onChangeSearch = e => {
    console.log(e);
  };

  // ฟังชั่น อัพโหลดข้อมูล จาก  form submit
  const submitFormFood = values => {
    const CheckIdEditValue = values.unit_id;
    if (values) {
      if (CheckIdEditValue) {
       // console.log({values})
        updateFoodUnit( values, CheckIdEditValue);
        setShowDialog(false);
      } else {
        //เพิ่มข้อมูลใหม่
        createFoodUnit(values);
        setShowDialog(false);
        // if (!foodUnitReducer.isError) {
        //   swal("สำเร็จ", "คุณเพิ่มข้อมูลรายการอาหารเสร็จสิ้น!", "success");
        // }
      }
    }
  };

  // if (foodUnitReducer.isStatus) {

  //   // swal("d", {
  //   //   icon: "success"

  //   // });
  // }

  return (
    <React.Fragment>
      {foodUnitReducer.isFetching ? (
        <div className={classes.root}>
          <LinearProgress />
        </div>
      ) : (
          <div className={classes.root}>
            <FormDialogFoodUnit
              initialValues={initialValues}
              onSubmit={submitFormFood}
              handleOpenDialog={showDialog}
              handleCloseDialog={e => closeDialogForm(e)}
            />
            <Paper className={classes.paper}>
              <EnhancedTableToolbar
                onChangeSearch={e => onChangeSearch(e)}
                openDialogForm={e => openDialogForm(e)}
              />

              <div className={classes.tableWrapper}>
                <Table
                  className={classes.table}
                  aria-labelledby="tableTitle"
                  size={"medium"}
                >
                  <EnhancedTableHead
                    classes={classes}
                    order={order}
                    orderBy={orderBy}
                    onRequestSort={handleRequestSort}
                    rowCount={foodUnitReducer.payload.length}
                  />
                  <TableBody>
                    {stableSort(
                      foodUnitReducer.payload,
                      getSorting(order, orderBy)
                    )
                      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      .map((row, index) => {
                        const labelId = `enhanced-table-checkbox-${index}`;
                        return (
                          <TableRow tabIndex={-1} key={row.unit_id}>
                            <TableCell
                              component="th"
                              id={labelId}
                              scope="row"
                              padding="default"
                            >
                              {row.unit_id}
                            </TableCell>
                            <TableCell align="right">{row.unit_name}</TableCell>
                            <TableCell align="right">{row.active_flag ? "แสดงผล" : "ไม่แสดงผล"}</TableCell>

                            <TableCell align="right">
                              <Tooltip title="Edit">
                                <IconButton
                                  aria-label=""
                                  onClick={e => {
                                    openDialogForm(e, row, true);
                                  }}
                                >
                                  <Build />
                                </IconButton>
                              </Tooltip>

                              <Tooltip title="Delete">
                                <IconButton
                                  aria-label="delete"
                                  onClick={e => {
                                    deleteDataRows(row.unit_id);
                                  }}
                                >
                                  <Delete />
                                </IconButton>
                              </Tooltip>
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    {emptyRows > 0 && (
                      <TableRow style={{ height: 49 * emptyRows }}>
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </div>
              <TablePagination
                rowsPerPageOptions={[20, 25]}
                component="div"
                count={foodUnitReducer.payload.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  "aria-label": "previous page"
                }}
                nextIconButtonProps={{
                  "aria-label": "next page"
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />
            </Paper>
          </div>
        )}
    </React.Fragment>
  );
};

const mapStateToProps = ({ foodUnitReducer }) => ({ foodUnitReducer });

const mapDispatchToProps = {
  getAllFoodUnit,
  createFoodUnit,
  updateFoodUnit,
  deleteFoodUnit
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryFood);
